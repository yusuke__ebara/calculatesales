package jp.alhinc.ebara_yusuke.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class CalculateSales {


	public static void main(String[] args) {
		Map<String,String> branchNames = new HashMap<>();
		Map<String,Long> branchSales = new HashMap<>();

		BufferedReader br = null;
		BufferedWriter bw = null;
		try {
			File file1 = new File(args[0],"branch.lst");
			FileReader fr = new FileReader(file1);
			br = new BufferedReader(fr);
			String line;
			while((line = br.readLine()) != null) {
				String[] splitName = line.split(",");
				branchNames.put(splitName[0], splitName[1]);
				branchSales.put(splitName[0], 0L);
				}
		}catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
		}finally {
			if(br != null) {
				try {
					br.close();
				}catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
				}
			}
		}

		File[] files = new File(args[0]).listFiles();
		List<File> rcdFiles = new ArrayList<>();
		for(int i = 0; i < files.length; i++) {
			String fileNames = files[i].getName();
			if(fileNames.matches("^[0-9]{8}+.+rcd$")) {
				rcdFiles.add(files[i]);
			}
		}
		//フォルダの中のrcdファイルのみを格納

		BufferedReader br2 = null;
			for(int i = 0; i < rcdFiles.size(); i++) {
				File f = rcdFiles.get(i);
				try {
					FileReader fr2 = new FileReader(f);
					br2 = new BufferedReader(fr2);
					List<String> lineList = new ArrayList<>();
					String readFile;
					while((readFile = br2.readLine()) != null) {
						lineList.add(readFile);
					}
					String num = lineList.get(0);
					String sale = lineList.get(1);
					Long longSale = Long.parseLong(sale);
					System.out.println(num + "," + sale);

					//numとsaleに格納されているもの

					if(branchSales.containsKey(num)) {
						longSale += branchSales.get(num);
					}
					branchSales.put(num, longSale);

				} catch (IOException e) {
					System.out.println("予期せぬエラーが発生しました");
				}finally {
					if(br != null) {
						try {
							br.close();
						}catch(IOException e) {
							System.out.println("予期せぬエラーが発生しました");
						}
					}
				}
			}
			System.out.println(branchSales.entrySet());
			System.out.println(branchSales.get("001"));
			System.out.println(branchSales.get("002"));
			System.out.println(branchSales.get("003"));
			System.out.println(branchSales.get("004"));
			System.out.println(branchSales.get("005"));
			//branchSalesに格納されているか確認


		try {
			File file3 = new File(args[0],"branch.out");
			FileWriter fw = new FileWriter(file3,true);
			bw = new BufferedWriter(fw);
			PrintWriter pw = new PrintWriter(bw);
			pw.println("001" + "," + branchNames.get("001") + "," +  branchSales.get("001"));
			pw.println("002" + "," + branchNames.get("002") + "," +  branchSales.get("002"));
			pw.println("003" + "," + branchNames.get("003") + "," +  branchSales.get("003"));
			pw.println("004" + "," + branchNames.get("004") + "," +  branchSales.get("004"));
			pw.println("005" + "," + branchNames.get("005") + "," +  branchSales.get("005"));

			pw.close();
		}catch(IOException e){
			System.out.println("予期せぬエラーが発生しました");
		}
	}
}